\select@language {french}
\contentsline {section}{\numberline {1}Les boucles}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}tant que}{2}{subsection.1.1}
\contentsline {subparagraph}{Afficher les nombres plus petits que 10}{2}{section*.3}
\contentsline {subparagraph}{Somme de nombres}{3}{section*.4}
\contentsline {subsection}{\numberline {1.2}while}{4}{subsection.1.2}
\contentsline {subparagraph}{Les puissances de 2}{4}{section*.5}
\contentsline {subparagraph}{Somme d'entiers positifs}{4}{section*.6}
\contentsline {subparagraph}{Somme d'entiers positifs}{5}{section*.7}
\contentsline {subparagraph}{Un exemple complet}{5}{section*.8}
\contentsline {subsection}{\numberline {1.3}faire - jusqu'\`a ce que}{6}{subsection.1.3}
\contentsline {subparagraph}{Somme de nombres}{7}{section*.10}
\contentsline {subsection}{\numberline {1.4}do...while}{8}{subsection.1.4}
\contentsline {subparagraph}{Les puissances de 2}{9}{section*.11}
\contentsline {subparagraph}{Somme d'entiers positifs}{9}{section*.12}
\contentsline {subsection}{\numberline {1.5}pour}{9}{subsection.1.5}
\contentsline {subparagraph}{Afficher les nombres plus petits que n}{11}{section*.14}
\contentsline {subparagraph}{Afficher les nombres pairs plus petits que 10}{11}{section*.15}
\contentsline {subparagraph}{Afficher les nombres pairs plus petits que n}{12}{section*.16}
\contentsline {subparagraph}{Afficher n nombres pairs}{12}{section*.17}
\contentsline {subparagraph}{Somme de nombres}{12}{section*.18}
\contentsline {subsection}{\numberline {1.6}for}{13}{subsection.1.6}
\contentsline {subparagraph}{Les nombres pairs entre 2 et 100}{13}{section*.19}
\contentsline {subparagraph}{Compte \`a rebours}{13}{section*.20}
\contentsline {subsection}{\numberline {1.7}Quel type de boucle choisir ?}{14}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Sentinelle}{14}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}suite de nombres}{15}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}3 pas en avant, 2 pas en arri\`ere}{16}{subsection.1.10}
\contentsline {section}{\numberline {2}La gestion des erreurs : try - catch}{17}{section.2}
\contentsline {subsection}{\numberline {2.1}L'instruction try catch}{17}{subsection.2.1}
\contentsline {section}{\numberline {3}Exercices}{18}{section.3}
\contentsline {subsection}{\numberline {3.1}Compr\'ehension d'algorithme}{18}{subsection.3.1}
\contentsline {subparagraph}{Compr\'ehension}{18}{section*.22}
\contentsline {subsection}{\numberline {3.2}Compr\'ehension de codes Java}{20}{subsection.3.2}
\contentsline {subparagraph}{Instructions r\'ep\'etitives}{20}{section*.23}
\contentsline {subparagraph}{Activit\'e 'remplir les blancs'}{21}{section*.24}
\contentsline {subparagraph}{Exp\'erience}{21}{section*.25}
\contentsline {subparagraph}{Compr\'ehension}{21}{section*.26}
\contentsline {subparagraph}{Exercice Tant que}{21}{section*.27}
\contentsline {subparagraph}{Exercice Pour}{22}{section*.28}
\contentsline {subparagraph}{Mots cl\'es}{22}{section*.29}
\contentsline {subsection}{\numberline {3.3}\`A vous de jouer...}{25}{subsection.3.3}
\contentsline {subparagraph}{Jeu de la fourchette}{26}{section*.30}
